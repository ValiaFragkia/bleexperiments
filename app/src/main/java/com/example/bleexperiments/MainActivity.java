package com.example.bleexperiments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bleexperiments.sensors.SensorsService;
import com.example.bleexperiments.sensors.helpers.Biosignal;
import com.example.bleexperiments.sensors.helpers.BluetoothHelper;
import com.example.bleexperiments.sensors.helpers.BpmMeasurement;
import com.example.bleexperiments.sensors.helpers.Comment;
import com.example.bleexperiments.sensors.helpers.Measurement;
import com.example.bleexperiments.sensors.helpers.OximeterMeasurement;
import com.example.bleexperiments.sensors.helpers.SensorData;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.google.gson.Gson;

import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class MainActivity extends Activity {

    private final static String TAG = MainActivity.class.getSimpleName();

    private static final int SCAN_ITERATIONS = 5;
    private static final long RETURN_PERIOD = 15000; //15Sec

    //UI Elements
    private TextView mStatusTextView;
    private TextView Spo2TextView;
    private TextView BpmTextView;
    private TextView PiTextView;
    private Button mButtonCancel;
    private LineChart mChart;
    private String mail;

    //Countdown
    private CountDownTimer mReturnTimer;

    private boolean sensorServiceActive = false;
    private int scanFails = 0;
    private SensorData lastSensorData;
    private int current_position = 0;
    private int oximeterEnabled = 0;

    private List<Biosignal> biosignals = new ArrayList<Biosignal>();
    private Comment comment;

    //Generates a unique uuid. Use it in both the biosignals and the comments
    private String uuid = null;

    public static final String PREFS_SAVE = "MyPrefsFile";

    private BroadcastReceiver sensorReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            SensorData sensorData = (SensorData) intent.getSerializableExtra(SensorsService.SENSOR_DATA);
            switch (intent.getAction()) {
                case SensorsService.INTENT_SENSORS_START_SCAN:
                    break;
                case SensorsService.INTENT_SENSORS_SCAN_FAILED:
                    if (oximeterEnabled ==1) {
                        if (++scanFails < SCAN_ITERATIONS) {
                            Log.i(TAG, "Scan fail #" + scanFails + ". Restarting...");
                            startScan();
                        } else {
                            Log.i(TAG, "Scan fail #" + scanFails + ". Finishing...");
                            finishActivity();
                        }
                    }
                    break;
                case SensorsService.INTENT_SENSORS_CONNECTED:
                    mStatusTextView.setText("Connected\n");
                    break;
                case SensorsService.INTENT_SENSORS_MEASURING:
                    mStatusTextView.setText("Measuring\n");
                    lastSensorData = sensorData;
                    showMeasurement(sensorData);
                    break;
                case SensorsService.INTENT_SENSORS_DISCONNECTED:
                    //This check is required to ignore the disconnect messages after measuring is finished
                    if (sensorServiceActive) {
                        stopScan();

                        Log.i(TAG, "Disconnected...");

                        mStatusTextView.setText("Disconnected\n");

                        //Restart if disconnected during connecting...
                        if (++scanFails < SCAN_ITERATIONS) {
                            Log.i(TAG, "Scan fail #" + scanFails + ". Restarting...");
                            startScan();
                        }
                    }
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        uuid = UUID.randomUUID().toString();

        super.onCreate(savedInstanceState);

        setContentView(R.layout.login_page);

        // Restore preferences
        SharedPreferences settings = getSharedPreferences(PREFS_SAVE, 0);
        String lastUserMail= settings.getString("saved mail" ,"");
        setMail(lastUserMail);

    }

    public void setMail(String loginMail){

        EditText login_info = (EditText) findViewById(R.id.login_mail);
        login_info.setText(loginMail);
    }

    public void onButtonlogIn(View view) {

        // print to log the e-mail
        EditText login_info = (EditText) findViewById(R.id.login_mail);
        mail = String.valueOf(login_info.getText());
        if (login_info.length() == 0) { //error message in blank input
            Log.d(TAG, "errorMessage: Blank Input");

            //show error message
            Toast toast = Toast.makeText(getApplicationContext(), "Please type in your e-mail to log in", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 400);
            TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
            v.setTextColor(Color.WHITE);
            toast.getView().setBackgroundColor(Color.DKGRAY);
            toast.show();
        } else {
            setContentView(R.layout.monitor_values_before);
            Log.d(TAG, "mail login: " + mail);

            // We need an Editor object to make preference changes.
            // All objects are from android.context.Context
            SharedPreferences settings = getSharedPreferences(PREFS_SAVE, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString("saved mail", mail);

            // Commit the edits!
            editor.commit();

        }
    }

    public void onButtonToOximeter(View view) {

        // print to log the blood pressure monitor values before
        EditText systPressBefore = (EditText) findViewById(R.id.systolic_press);
        EditText diastPressBefore = (EditText) findViewById(R.id.diast_pressure);
        EditText bpmBefore = (EditText) findViewById(R.id.beats_pm);

        String systolic = String.valueOf(systPressBefore.getText());
        String diastolic = String.valueOf(diastPressBefore.getText());
        String heartRate = String.valueOf(bpmBefore.getText());

        if ((systPressBefore.length() == 0) || (diastPressBefore.length() == 0)
                || (heartRate.length() == 0)) { //error message in blank input
            Log.d(TAG, "errorMessage: Blank Input");

            //show error message
            Toast toast = Toast.makeText(getApplicationContext(), "Please fill in the form with your blood pressure values",
                    Toast.LENGTH_LONG);
            toast.setGravity(Gravity.TOP | Gravity.CENTER, 0, 150);
            TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
            v.setTextColor(Color.WHITE);
            toast.getView().setBackgroundColor(Color.DKGRAY);
            toast.show();
        } else {
            oximeter_measure();
            Log.d(TAG, "systolic pressure before: " + systolic);
            Log.d(TAG, "diastolic pressure before: " + diastolic);
            Log.d(TAG, "beats per minute before: " + heartRate);

            BpmMeasurement  firstMonitorValuesToSave = createBpmMeasurement(systolic, diastolic, heartRate);

           // create and add biosignal to list_of_biosignals
            Biosignal biosignal  = new Biosignal(null, uuid, mail, new Date(), "bpm", null, firstMonitorValuesToSave);
            biosignals.add(biosignal);
        }
    }

    public void onButtonSubmit(View view) {

        // print to log the blood monitor values after the oximeter measurement
        EditText systPressAfter = (EditText) findViewById(R.id.systolic_press);
        EditText diastPressAfter = (EditText) findViewById(R.id.diast_pressure);
        EditText bpmAfter = (EditText) findViewById(R.id.beats_pm);

        String systolic = String.valueOf(systPressAfter.getText());
        String diastolic = String.valueOf(diastPressAfter.getText());
        String heartRate = String.valueOf(bpmAfter.getText());

        if ((systPressAfter.length() == 0) || (diastPressAfter.length() == 0)
                || (heartRate.length() == 0)) { //error message in blank input
            Log.d(TAG, "errorMessage: Blank Input");

            //show error message
            Toast toast = Toast.makeText(getApplicationContext(), "Please fill in the form with your blood pressure values",
                    Toast.LENGTH_LONG);
            toast.setGravity(Gravity.TOP | Gravity.CENTER, 0, 150);
            TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
            v.setTextColor(Color.WHITE);
            toast.getView().setBackgroundColor(Color.DKGRAY);
            toast.show();
        } else {
            Log.d(TAG, "systolic pressure after: " + systolic);
            Log.d(TAG, "diastolic pressure after: " + diastolic);
            Log.d(TAG, "beats per minute after: " + heartRate);

            BpmMeasurement  lastMonitorValuesToSave = createBpmMeasurement(systolic, diastolic, heartRate);


            Biosignal biosignal  = new Biosignal(null,uuid, mail, new Date(), "bpm", null, lastMonitorValuesToSave);

            biosignals.add(biosignal);

            //print biosignal list to log
            Log.d(TAG,"Size_biosignals: " +biosignals.size());
            for (int i=0;i<biosignals.size();i++) {
                Log.d(TAG, "ValiaPrintBiosignals: " + String.valueOf(biosignals.get(i)));
            }
            // POST REQUEST to client

            setContentView(R.layout.final_submit);
        }
    }

    public void onButtonSave(View view){
        //save session
        Log.d(TAG,"Save Session");
        EditText commentMessage = (EditText) findViewById(R.id.comment_submit);
        String message = String.valueOf(commentMessage.getText());

        if (commentMessage.length() == 0){//blank input
            //show error message
            Toast toast = Toast.makeText(getApplicationContext(), "Please type in your current physical state",
                    Toast.LENGTH_LONG);
            toast.setGravity(Gravity.TOP | Gravity.CENTER, 0, 230);
            TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
            v.setTextColor(Color.WHITE);
            toast.getView().setBackgroundColor(Color.DKGRAY);
            toast.show();
        } else {
            Log.d(TAG, "physical state Comment: " + message);

            //create new Comment
            comment = new Comment(null, uuid, mail, new Date(), message);

            //Connect to Medical Server
            new DownloadFilesTask().execute();

            finishActivity();
        }
    }

    public void onButtonDiscard(View view) {
        //do not save session
        Log.d(TAG,"Discard Session");
        finishActivity();
    }

    public BpmMeasurement createBpmMeasurement(String systolic, String diastolic, String heartRate) {

        int systolicValue = Integer.parseInt(systolic);
        int diastolicValue = Integer.parseInt(diastolic);
        int heartRateValue = Integer.parseInt(heartRate);

        Measurement systolicToSave = new Measurement("mmHg", systolicValue);
        Measurement diastolicToSave = new Measurement("mmHg", diastolicValue);
        Measurement bpmToSave = new Measurement("beats/min", heartRateValue);

        BpmMeasurement monitorValuesToSave = new BpmMeasurement(systolicToSave, diastolicToSave, bpmToSave);

        return monitorValuesToSave;
    }

    public OximeterMeasurement createOximeterMeasurement(int oxygenSaturation,int heartRate,int pi, List<Integer> ppg){

        Measurement saturationToSave = new Measurement("mmHg", oxygenSaturation);
        Measurement heartRateToSave = new Measurement("beats/min", heartRate);
        Measurement piToSave = new Measurement("%", pi);

        OximeterMeasurement oximeterValuesToSave = new OximeterMeasurement(saturationToSave,heartRateToSave,piToSave,ppg);

        return oximeterValuesToSave;
    }

    public void oximeter_measure() {

        oximeterEnabled = 1;
        Log.d(TAG, "Oximeter Enabled on oximetermeasure(): " + oximeterEnabled);
        setContentView(R.layout.oximeter_screen);

        // Sets up UI references.
        mStatusTextView = (TextView) findViewById(R.id.status_textView);
        Spo2TextView = (TextView) findViewById(R.id.spo2_textView);
        BpmTextView = (TextView) findViewById(R.id.bpm_textView);
        PiTextView = (TextView) findViewById(R.id.pi_textView);
        mButtonCancel = (Button) findViewById(R.id.cancel_button);

        mChart = (LineChart) findViewById(R.id.chart);
        mChart.setDrawGridBackground(false);
        mChart.setDescription("");

        // enable touch gestures
        mChart.setTouchEnabled(true);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);
        mChart.setDrawGridBackground(false);

        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(true);

        // set an alternative background color
        mChart.setBackgroundColor(Color.LTGRAY);

            /*Axis*/

        YAxis yAxis = mChart.getAxisLeft();
//            leftAxis.setTypeface(tf);
        yAxis.setAxisMaxValue(100f);
        yAxis.setAxisMinValue(0f);
        yAxis.setDrawGridLines(false);
        yAxis.setDrawLabels(false);

        YAxis yAxis2 = mChart.getAxisRight();
        yAxis2.setEnabled(false);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawLabels(false);

        List<Entry> list = new ArrayList<>();
        LineDataSet dataSet = new LineDataSet(list, "ppg");
        List<LineDataSet> dataSets = new ArrayList<>();
        dataSets.add(dataSet);

        dataSet.setColor(Color.BLUE);
        LineData data = new LineData();
        data.setHighlightEnabled(false);

        // add empty data
        mChart.setData(data);
        mChart.invalidate();
        activateScan();
    }

    public void onButtonFinish(View view) {

         //disconnect the oximeter
        if (sensorServiceActive) {
            stopScan();
            setReturnTimer(false);
            sensorServiceActive = false;
            oximeterEnabled = 0;
        }
        Log.d(TAG, "Disconnect oximeter measurement ");
        setContentView(R.layout.monitor_values_after);
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume Called");

        super.onResume();
        Log.d(TAG, "OximeterEnabled on Resume : " + oximeterEnabled);
        if (oximeterEnabled == 1) {     //control when the oximeter measurement starts-ButtonOximeter only
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

            IntentFilter filter = new IntentFilter();
            filter.addAction(SensorsService.INTENT_SENSORS_START_SCAN);
            filter.addAction(SensorsService.INTENT_SENSORS_SCAN_FAILED);
            filter.addAction(SensorsService.INTENT_SENSORS_CONNECTED);
            filter.addAction(SensorsService.INTENT_SENSORS_MEASURING);
            filter.addAction(SensorsService.INTENT_SENSORS_DISCONNECTED);
            LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(sensorReceiver, filter);

            //  new DownloadFilesTask().execute();
            startScan();
            Log.d(TAG, "Scan Start");
        }
    }

    protected void activateScan() {
        Log.d(TAG, "onResume Called");

        super.onResume();
        Log.d(TAG, "OximeterEnabled on Resume : " + oximeterEnabled);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        IntentFilter filter = new IntentFilter();
        filter.addAction(SensorsService.INTENT_SENSORS_START_SCAN);
        filter.addAction(SensorsService.INTENT_SENSORS_SCAN_FAILED);
        filter.addAction(SensorsService.INTENT_SENSORS_CONNECTED);
        filter.addAction(SensorsService.INTENT_SENSORS_MEASURING);
        filter.addAction(SensorsService.INTENT_SENSORS_DISCONNECTED);
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(sensorReceiver, filter);

        startScan();
        // Log.d(TAG, "Scan Start111");
    }


    @Override
    protected void onPause() {
        Log.d(TAG, "onPause Called");

        super.onPause();
        if (oximeterEnabled == 1) {

            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

            LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(sensorReceiver);

            stopScan();
        }
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed");

//        finishActivity();

    }

    private void setReturnTimer(boolean enable) {
        //cancel counter if exists and start a new to stop scanning
        if (mReturnTimer != null) {
            mReturnTimer.cancel();
            mReturnTimer = null;
        }
        if (enable) {
            mReturnTimer = new CountDownTimer(RETURN_PERIOD, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                }

                @Override
                public void onFinish() {
                    Log.d(TAG, "Return timer onFinish");

                    //Check if a new measurement is available and present it, otherwise finish
                    if (lastSensorData != null &&
                            System.currentTimeMillis() - lastSensorData.getDate().getTime() < 2000) {
                        setReturnTimer(true);
                        showMeasurement(lastSensorData);
                    } else {
                        finishActivity();
                    }
                }
            }.start();
        }
    }

    private void startScan() {
            Log.d(TAG, "startScan Called");

            setReturnTimer(false);
            sensorServiceActive = true;

            Intent intent = new Intent(getApplicationContext(), SensorsService.class);
            intent.setAction(SensorsService.INTENT_SENSORS_START_SCAN);
            startService(intent);

            mStatusTextView.setText(R.string.scanning);
        }

        private void stopScan() {
            Log.d(TAG, "stopScan Called");

            Intent intent = new Intent(getApplicationContext(), SensorsService.class);
            intent.setAction(SensorsService.INTENT_SENSORS_STOP_ALL);
            startService(intent);

            sensorServiceActive = false;


            mStatusTextView.setText(R.string.stopped);
        }

        private void finishActivity() {
            Log.d(TAG, "finishActivity Called");

            finish();
        }

        private void showMeasurement(SensorData sensorData) {
            Spo2TextView.setText(BluetoothHelper.sensorDataPrintFormat_spo2(sensorData));
            BpmTextView.setText(BluetoothHelper.sensorDataPrintFormat_bpm(sensorData));
            PiTextView.setText(BluetoothHelper.sensorDataPrintFormat_pi(sensorData));


            updatePPG(sensorData.getPpg());

            //create biosignal
            int [] ppg=sensorData.getPpg();

         //   int j =0;
            List<Integer> ppg_list = new ArrayList<Integer>();
            for (int i = 0; i < ppg.length; i++){
                ppg_list.add(ppg[i]);
                Log.d(TAG,"PPG: "+ppg[i]);
               // if (ppg[i]==0)
                  //  j++;
            }

            Log.d(TAG,"test : Spo2, Bmp, Pi :"+ sensorData.getSpo2()+ " " +sensorData.getBpm()+ " " + sensorData.getPi());
            int pi = (int) (10 * sensorData.getPi());

            if ((sensorData.getBpm() != 0) && ppg.length!=0) { //not dummy data
                OximeterMeasurement OximeterValuesToSave = createOximeterMeasurement(sensorData.getSpo2(), sensorData.getBpm(),
                        pi, ppg_list);

                Biosignal biosignal = new Biosignal(null,uuid, mail, new Date(), "oximeter", OximeterValuesToSave, null);

                //add biosignal to list of biosignals
                biosignals.add(biosignal);
            }
        }

        private void updatePPG(int[] ppg) {

//        List<Entry> list = new ArrayList<>();
            //      LineDataSet dataSet = new LineDataSet(list, "ppg");

            LineData data2 = mChart.getData();

            //150 8eseis gia 3 deuterolepta

            if (data2 != null) {
//           LineDataSet set = data2.getDataSetByIndex(0);

                int count = (data2.getDataSetCount());
                Log.d(TAG, String.valueOf("valia dataset_count: " + count));
                int num_x = data2.getXValCount();
                Log.d(TAG, String.valueOf("num_x: " + num_x));

                //1st case
                if (data2.getXValCount() < 150) {
                    Log.d(TAG, "eisodos1");

                    ArrayList<Entry> yVals = new ArrayList<Entry>();

                    // add 10 x-entries
                    Log.d(TAG, String.valueOf("index: " + num_x));
                    int diff = 150 - num_x;
                    if (diff < ppg.length) {
                        for (int i = 0; i < diff; i++) {
                            data2.addXValue("" + i);
                            Log.d(TAG, String.valueOf(data2.getXValCount()));
                            Log.d(TAG, String.valueOf("dif1: " + diff));

                            int cur_pos = num_x + i;
                            yVals.add(new Entry(ppg[i], cur_pos));
                         //   Log.d(TAG, "i = " + i + "  ppg[i] = " + ppg[i] + " curr_position= " + cur_pos);
                        }
                        current_position = 0;
                        for (int i = 0; i < (ppg.length - diff); i++) {
                            int cur_pos = current_position + i;
                            yVals.add(new Entry(ppg[i], cur_pos));
                           // Log.d(TAG, "i = " + i + "  ppg[i] = " + ppg[i] + " curr_position= " + cur_pos);
                        }
                    } else { //normal case
                        for (int i = 0; i < ppg.length; i++) {
                            data2.addXValue("" + i);
                            Log.d(TAG, String.valueOf(data2.getXValCount()));
                            Log.d(TAG, String.valueOf("dif2: " + diff));
                            int cur_pos = num_x + i;
                            yVals.add(new Entry(ppg[i], cur_pos));
                            //Log.d(TAG, "i = " + i + "  ppg[i] = " + ppg[i] + " curr_position= " + cur_pos);
                        }
                    }

                    LineDataSet set = new LineDataSet(yVals, "DataSet ");
                    data2.addDataSet(set);

                    LineData data_show = new LineData(data2.getXVals(), data2.getDataSets());

                    mChart.setData(data_show);
                    set.setColor(Color.BLUE);
                    set.setDrawValues(false);
                    set.setCircleSize(0f);

                    mChart.notifyDataSetChanged(); // let the chart know its data changed
                    mChart.invalidate();
                } else if (data2.getXValCount() == 150) {  //2nd case
                    Log.d(TAG, "eisodos2");
                    Log.d(TAG, "ppg.length: " + ppg.length);

                    data2 = mChart.getData();

                /*remove oldest dataset*/
                    data2.removeDataSet(0);

                    Log.d(TAG, String.valueOf("DataSetCount_NEW: " + data2.getDataSetCount()));

                    ArrayList<Entry> yVals = new ArrayList<Entry>();

                /*add new ppg entries in the end*/
                    int diff = 150 - current_position;
                    if (diff >= ppg.length) {
                        for (int i = 0; i < ppg.length; i++) {
                            int cur_pos = current_position + i;
                            yVals.add(new Entry(ppg[i], cur_pos));
//                            Log.d(TAG, "i = " + i + "  ppg[i] = " + ppg[i] + " curr_position= " + cur_pos);
                        }
                        current_position += ppg.length;
                        if (current_position >= 150)
                            current_position = 0;
  //                      Log.d(TAG, "current position_after_renewal : " + current_position);
                    } else {  //diff < ppg.length
                        for (int i = 0; i < diff; i++) {
                            int cur_pos = current_position + i;
                            yVals.add(new Entry(ppg[i], cur_pos));
    //                        Log.d(TAG, "i = " + i + "  ppg[i] = " + ppg[i] + " curr_position= " + cur_pos);
                        }
                        current_position = 0;
                        for (int i = 0; i < (ppg.length - diff); i++) {
                            int cur_pos = current_position + i;
                            yVals.add(new Entry(ppg[i], cur_pos));
      //                      Log.d(TAG, "i = " + i + "  ppg[i] = " + ppg[i] + " curr_position= " + cur_pos);
                        }
                        current_position += (ppg.length - diff);
                    }
                    LineDataSet set = new LineDataSet(yVals, "DataSet ");
                    data2.addDataSet(set);

                    LineData data_show = new LineData(data2.getXVals(), data2.getDataSets());

                    mChart.setData(data_show);
                    set.setColor(Color.BLUE);
                    set.setDrawValues(false);
                    set.setCircleSize(0f);

                    mChart.notifyDataSetChanged(); // let the chart know it's data changed
                    mChart.invalidate();
                }
            }
        }

    public void onCancelClicked(View view) {
        Log.d(TAG, "Cancel Pressed");

        finishActivity();
    }


    private class DownloadFilesTask extends AsyncTask<URL, Integer, String> {
        protected String doInBackground(URL... urls) {

                Gson gson = new Gson();

                for(Biosignal b : biosignals) {
                    System.out.println(gson.toJson(b));
                }

                /* The connection URL
                one biosignal
                String url = "http://83.212.117.117:8080/biosignals/biosignals";*/

                //many biosignals
                String url = "http://83.212.117.117:8080/biosignals/custombiosignals";

                String urlComments = "http://83.212.117.117:8080/biosignals/comments";

                // Create a new RestTemplate instance
                try {
                    RestTemplate restTemplate = new RestTemplate();

                    // Add the String message converter
                    restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

                    // Make the HTTP GET request, marshaling the response a String
                    // String result = restTemplate.getForObject(url, String.class);

                    //Post Request
                    restTemplate.postForObject(url, biosignals, String.class);

                    restTemplate.postForObject(urlComments, comment, String.class);

                }catch(RuntimeException e){
                    //show error message
                    Log.d(TAG, "Runtime exception, An error occurred");
                    finishActivity();
                }
                return url;
            }

            protected void onProgressUpdate(Integer... progress) {
            }

            protected void onPostExecute(String result) {
                mStatusTextView.setText(result);
                Log.e(TAG, result);
            }
        }
    }


