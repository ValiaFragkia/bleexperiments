package com.example.bleexperiments.sensors.drivers.oximeters;

import android.bluetooth.BluetoothGattDescriptor;
import android.util.Log;

import com.example.bleexperiments.MainActivity;
import com.example.bleexperiments.sensors.drivers.BleDriver;
import com.example.bleexperiments.sensors.helpers.SensorData;

import java.io.OutputStream;
import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;
import java.util.UUID;

/**
 * Created by amenychtas on 26/4/2015.
 */

public class MedicalDriver implements BleDriver {
    private static final String TAG = MedicalDriver.class.getSimpleName();

    public static final String NAME = "Medical";
    private static final String DRAWABLE = "icon_sensor_medical";

    private static final UUID SERVICE_UUID =
            UUID.fromString("CDEACB80-5235-4C07-8846-93A37EE6B86D");
    private static final UUID CHARACTERISTIC_UUID =
            UUID.fromString("cdeacb81-5235-4c07-8846-93a37ee6b86d");

    private static final int STABLE_MEASUREMENT_COUNT = 4;

    private Queue<Integer> spo2MeasurementsQueue = new LinkedList<>();
    private Queue<Integer> pulseMeasurementsQueue = new LinkedList<>();
    private int spo2Sum = 0;
    private int pulseSum = 0;

    private int spo2 = 0;
    private int bpm=0;
    private double pi=0;
    private int sum=0;
    private int [] ppg;

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDrawable() {
        return DRAWABLE;
    }

    @Override
    public boolean isAutoConnect() {
        return false;
    }

    @Override
    public UUID getServiceUuid() {
        return SERVICE_UUID;
    }

    @Override
    public UUID getCharacteristicUuid() {
        return CHARACTERISTIC_UUID;
    }

    @Override
    public byte[] getDescriptor() {
        return BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE;
    }

    @Override
    public boolean preMeasurement(OutputStream outputStream) {
        return true;
    }

    @Override
    public GattData preMeasurement() {
        return null;
    }

    @Override
    public SensorData processData(byte[] data, String deviceName, String deviceAddress) { //process paketa
        int num;
        int[] ppg_local = new int[10]; //local ppg gia euresh sum kai ignore invalid data
        sum=0;

        SensorData sensorData = new SensorData(
                deviceName,
                deviceAddress,
                new Date(), spo2, bpm, pi, ppg);


        if ((data != null)
                && (data.length == 4)
                && (data[0] == (byte) 0x81)) {

            int cur02Measurement = data[2] & 0xff;
            int curPulseMeasurement = data[1] & 0xff;

            if (cur02Measurement >= 35 && cur02Measurement <= 100  //elegxos oti den einai skoupidi
                    && curPulseMeasurement >= 25 && curPulseMeasurement <= 250) {


                pi = (data[3] & 0xff) / 10.0;


                spo2 = cur02Measurement;
                bpm = curPulseMeasurement;

                sensorData.setSpo2(spo2);
                sensorData.setBpm(bpm);
                sensorData.setPi(pi);

                spo2MeasurementsQueue.offer(cur02Measurement);
                spo2Sum += cur02Measurement;
                if (spo2MeasurementsQueue.size() > STABLE_MEASUREMENT_COUNT) {
                    spo2Sum -= spo2MeasurementsQueue.poll();
                }

                pulseMeasurementsQueue.offer(curPulseMeasurement);
                pulseSum += curPulseMeasurement;
                if (pulseMeasurementsQueue.size() > STABLE_MEASUREMENT_COUNT) {
                    pulseSum -= pulseMeasurementsQueue.poll();
                }

                //Check if the values are stable - if they are not reset start time
                if (Math.abs(spo2Sum / STABLE_MEASUREMENT_COUNT - cur02Measurement) <= 1 &&
                        Math.abs(pulseSum / STABLE_MEASUREMENT_COUNT - curPulseMeasurement) <= 1) {
                    Log.d(TAG, "Stable Measurement"); // se ti xrhsimeuei
                } else {
                    Log.d(TAG, "Unstable Measurement");
                }
            }
        } else if ((data != null)
                && (data.length == 11)
                && (data[0] == (byte) 0x80)) {

            for (int i = 1; i < data.length; i++) {
                num = data[i] & 0xff;
                if (num <= 100) { //elegxos oti oi times einai <=100 k den einai 127
                    ppg_local[i - 1] = num;
                    sum += 1;
                }
            }

            ppg = new int[sum];   //gia na agnoountai oi invalid times
            for (int i = 0; i < ppg.length; i++) {
                ppg[i] = ppg_local[i];
            }

            sensorData.setSpo2(spo2);
            sensorData.setBpm(bpm);
            sensorData.setPi(pi);
            sensorData.setPpg(ppg);

        }
        return sensorData;
    }

    @Override
    public boolean postMeasurement(OutputStream outputStream) {
        return true;
    }

    @Override
    public GattData postMeasurement() {
        return null;
    }
}
