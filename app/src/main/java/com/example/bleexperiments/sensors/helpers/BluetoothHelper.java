package com.example.bleexperiments.sensors.helpers;

import android.util.Log;

import com.example.bleexperiments.MainActivity;
import com.example.bleexperiments.sensors.drivers.BleDriver;
import com.example.bleexperiments.sensors.drivers.oximeters.MedicalDriver;

import java.util.Properties;
import java.util.UUID;

/**
 * Created by amenychtas on 26/4/2015.
 */
public class BluetoothHelper {
    private static final String TAG = BluetoothHelper.class.getSimpleName();

    //Client = our app
    public static final UUID CLIENT_CHARACTERISTIC_CONFIG =
            UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

    private static Properties sensorsProperties;

    /**
     * This method reads the services supported by the device and returns the proper driver to
     * handle the communication with it. The method should be updated with the new devices each time.
     *
     * @param deviceName The device name for which the driver is needed.
     */
    public static synchronized BleDriver getDriver(String deviceName) {
        if (deviceName.equals(MedicalDriver.NAME)) {
            return new MedicalDriver();
        } else {
            return null;
        }
    }

    public static String sensorDataPrintFormat_spo2(SensorData sensorData) {
        StringBuilder sb = new StringBuilder();

                sb.append("Οξυγόνο: ");
                if(sensorData.getSpo2()!=0){
                    sb.append(Integer.toString(sensorData.getSpo2()));
                    sb.append("%");
                }
                sb.append('\n');

        return sb.toString();
    }

    public static String sensorDataPrintFormat_bpm(SensorData sensorData) {
        StringBuilder sb = new StringBuilder();

        sb.append("Σφυγμός: ");
        if (sensorData.getBpm() != 0) {
            sb.append(Integer.toString(sensorData.getBpm()));
            sb.append(" bpm");
        }
        sb.append('\n');

      return sb.toString();
    }

    public static String sensorDataPrintFormat_pi(SensorData sensorData) {
        StringBuilder sb = new StringBuilder();

        sb.append("PI: ");
        if (sensorData.getPi() != 0.0) {
            sb.append(Double.toString(sensorData.getPi()));
            sb.append("%");
        }
        sb.append('\n');

        return sb.toString();
    }

}
