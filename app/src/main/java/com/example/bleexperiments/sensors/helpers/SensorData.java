package com.example.bleexperiments.sensors.helpers;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by amenychtas on 14/4/2015.
 */
public class SensorData extends Sensor{
    private int[] ppg;
    private int spo2;
    private int bpm;
    private double pi;


    public SensorData(String deviceName, String deviceAddress, Date date, int spo2, int bpm, double pi, int[] ppg) {
        super(deviceName,deviceAddress, date);
        this.ppg = ppg;
        this.spo2 = spo2;
        this.bpm = bpm;
        this.pi = pi;
    }


    public int getSpo2() {
        return spo2;
    }

    public int getBpm() {
        return bpm;
    }

    public double getPi() {
        return pi;
    }

    public int[] getPpg() {
        return ppg;
    }

    public void setSpo2(int spo2) {
        this.spo2 = spo2;
    }

    public void setBpm(int bpm) {
        this.bpm = bpm;
    }

    public void setPi(double pi) {
        this.pi = pi;
    }

    public void setPpg(int[] ppg) {
        this.ppg = ppg;
    }

    }
