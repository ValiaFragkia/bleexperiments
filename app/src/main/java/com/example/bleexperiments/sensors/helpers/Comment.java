package com.example.bleexperiments.sensors.helpers;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

/**
 * Created by amenychtas on 29/11/2015.
 */
@JsonInclude(NON_NULL)
public class Comment {
    private String id;

    private String measurementId;

    private String userId;

    private Date date;
    private String message;

    public Comment() {
    }

    public Comment(String id, String measurementId, String userId, Date date, String message) {
        this.id = id;
        this.measurementId = measurementId;
        this.userId = userId;
        this.date = date;
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMeasurementId() {
        return measurementId;
    }

    public void setMeasurementId(String measurementId) {
        this.measurementId = measurementId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
