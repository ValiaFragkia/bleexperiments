package com.example.bleexperiments.sensors.helpers;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by amenychtas on 14/4/2015.
 */
public class Sensor implements Serializable {
    private final String deviceName;
    private final String deviceAddress;
    private final Date date;


    public Sensor(String deviceName, String deviceAddress,Date date) {
        this.deviceName = deviceName;
        this.deviceAddress = deviceAddress;
        this.date = date;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public String getDeviceAddress() {
        return deviceAddress;
    }

    public Date getDate() {
        return date;
    }
}
