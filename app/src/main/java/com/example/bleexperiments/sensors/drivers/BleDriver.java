package com.example.bleexperiments.sensors.drivers;

import com.example.bleexperiments.sensors.helpers.SensorData;

import java.io.OutputStream;
import java.util.UUID;

/**
 * Created by amenychtas on 29/4/2015.
 */
public interface BleDriver {
    /**
     * Returns the type of the device
     */
//    String getType();

    /**
     * Returns the name of the device, as specified in the driver.
     */
    String getName();

    /**
     * Returns the name of the device icon.
     */
    String getDrawable();

    /**
     * Indicates if andriod should autoconnect to the device or not
     */
    boolean isAutoConnect();

    /**
     * Returns the UUID of the BLE service
     */
    UUID getServiceUuid();

    /**
     * Returns the UUID of the BLE characteristic
     */
    UUID getCharacteristicUuid();

    /**
     * Returns the BLE descriptor value
     */
    byte[] getDescriptor();

    /**
     * This method sends the required messages to the device to be properly initialized
     */
    boolean preMeasurement(OutputStream outputStream);
    class GattData {
        public UUID SendCmd;
        public byte[] data;
    }

    /**
     * This method sends the required messages to the device to be properly initialized
     */
    GattData preMeasurement();


    /**
     * This method processes the data read from the characteristic.
     * If after reading the data the overall process should be stopped, the method returns true
     *
     * @param data          The byte array to read from
     * @param deviceName    The device name
     * @param deviceAddress The device address
     * @return The sensor data object with the processed data and indication if the
     * measurement process has been finished.
     */
    SensorData processData(byte[] data, String deviceName, String deviceAddress);

    /**
     * This method sends the required messages to the device to properly disconnect for the
     * Bluetooth devices
     */
    boolean postMeasurement(OutputStream outputStream);

    /**
     * This method sends the required messages to the device to properly disconnect for the
     * BLE devices
     */
    GattData postMeasurement();

}
