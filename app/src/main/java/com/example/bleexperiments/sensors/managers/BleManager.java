package com.example.bleexperiments.sensors.managers;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.content.Context;
import android.util.Log;

import com.example.bleexperiments.sensors.drivers.BleDriver;

/**
 * Created by amenychtas on 14/4/2015.
 */

public class BleManager {
    private static final String TAG = BleManager.class.getSimpleName();

    private final Context mContext;
    private BluetoothGatt mBluetoothGatt;

    //Constructor
    public BleManager(Context context) {
        mContext = context;
    }

    /**
     * Connects to a specific device
     *
     * @param device          The BLE device to connect.
     * @param bleDriver The driver instance that should be used to communicate with the device.
     */

    public void connect(BluetoothDevice device, BleDriver bleDriver) {
        Log.i(TAG, "Connecting to device " + device.getName() + " - " + device.getAddress());
        if (mBluetoothGatt != null) {
            disconnect();
        }
        // We want to directly connect to the device, so we are setting the autoConnect
        // parameter to false
        mBluetoothGatt = device.connectGatt(mContext, bleDriver.isAutoConnect(),
                BleCallback.getInstance(bleDriver));
    }

    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public void disconnect() {
        if (mBluetoothGatt != null) {
            try {
                mBluetoothGatt.disconnect();
            } catch (Exception e) {
            }
            mBluetoothGatt.close();
            mBluetoothGatt = null;
            Log.i(TAG, "BLE Devices Disconnected");
        }
    }
}
