package com.example.bleexperiments.sensors.managers;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.util.Log;

import com.example.bleexperiments.MainActivity;
import com.example.bleexperiments.sensors.SensorsService;
import com.example.bleexperiments.sensors.drivers.BleDriver;
import com.example.bleexperiments.sensors.helpers.BluetoothHelper;
import com.example.bleexperiments.sensors.helpers.Sensor;
import com.example.bleexperiments.sensors.helpers.SensorData;

import java.util.Date;

/**
 * Created by amenychtas on 29/4/2015.
 */
public class BleCallback extends BluetoothGattCallback {
    private static final String TAG = BleCallback.class.getSimpleName();

    private static BleCallback sBleCallback = new BleCallback();

    private static BleDriver sBleDriver;
    private SensorData mSensorData;
    private Sensor mSensor;
    private static long lastMeasurementTime = 0;

    private BleCallback() {
    }

    public static BleCallback getInstance(BleDriver bleDriver) {
        sBleDriver = bleDriver;
        lastMeasurementTime = 0;
        return sBleCallback;
    }

    @Override
    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
        if (newState == BluetoothProfile.STATE_CONNECTED) {
            Log.i(TAG, "Connected to device " + gatt.getDevice().getName() + " - " + gatt.getDevice().getAddress());

            mSensor = new Sensor(
                    gatt.getDevice().getName(),
                    gatt.getDevice().getAddress(),
                    new Date());
            SensorsService.sendMessage(SensorsService.SENSORS_CONNECTED_BLE, mSensorData);
            // Attempts to discover services after successful connection.
            gatt.discoverServices();
        } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
            Log.i(TAG, "Disconnected from device " + gatt.getDevice().getName() + " - " + gatt.getDevice().getAddress());
            SensorsService.sendMessage(SensorsService.SENSORS_DISCONNECTED, mSensorData);
        }
    }

    @Override
    public void onServicesDiscovered(BluetoothGatt gatt, int status) {
        if (status == BluetoothGatt.GATT_SUCCESS) {
            Log.i(TAG, "Found Services for device " + gatt.getDevice().getName() + " - " + gatt.getDevice().getAddress());

            BluetoothGattService gattService = gatt.getService(sBleDriver.getServiceUuid());
            if (gattService != null) {
                BluetoothGattCharacteristic gattCharacteristic =
                        gattService.getCharacteristic(sBleDriver.getCharacteristicUuid());
                if (gattCharacteristic != null) {
                    Log.i(TAG, "Setting Characteristic " + gattCharacteristic.getUuid());

                    //Disable notification before enabling it
                    gatt.setCharacteristicNotification(gattCharacteristic, false);
                    gatt.setCharacteristicNotification(gattCharacteristic, true);

                    BluetoothGattDescriptor clientDescriptor = gattCharacteristic.getDescriptor(
                            BluetoothHelper.CLIENT_CHARACTERISTIC_CONFIG);
                    clientDescriptor.setValue(sBleDriver.getDescriptor());
                    gatt.writeDescriptor(clientDescriptor);

                    //check if there is any pre measurement stuff to be done
                    BleDriver.GattData initData = sBleDriver.preMeasurement();
                    if (initData != null) {
                        Log.i(TAG, " Init Exists");
                        BluetoothGattCharacteristic gattCharacteristicSend = gattService.getCharacteristic(initData.SendCmd);
                        if (gattCharacteristicSend != null) {
                            Log.i(TAG, " Sending Send Cmd");
                            gattCharacteristicSend.setValue(initData.data);
                            gatt.writeCharacteristic(gattCharacteristicSend);
                        }

                    }

                    SensorsService.sendMessage(SensorsService.SENSORS_SERVICES_DISCOVERED, mSensorData);
                }
            }
        } else {
            Log.w(TAG, "onServicesDiscovered received: " + status);
        }
    }

    @Override
    public void onCharacteristicRead(BluetoothGatt gatt,
                                     BluetoothGattCharacteristic characteristic,
                                     int status) {
        if (status == BluetoothGatt.GATT_SUCCESS) {
            processCharacteristic(gatt, characteristic);
        }
    }

    @Override
    public void onCharacteristicChanged(BluetoothGatt gatt,
                                        BluetoothGattCharacteristic characteristic) {
        processCharacteristic(gatt, characteristic);
    }

    /**
     * Processes the characteristic data. It should be overridden for each sensor
     *
     * @param gatt           BluetoothGatt
     * @param characteristic BluetoothGattCharacteristic
     */
    public void processCharacteristic(BluetoothGatt gatt,
                                      BluetoothGattCharacteristic characteristic) {

        //Process only the relevant data
        if (!sBleDriver.getCharacteristicUuid().equals(characteristic.getUuid())) {
            return;
        }

        //Check if measurement has been received in the last 5sec, and if not, disconnect
        if (lastMeasurementTime == 0 || System.currentTimeMillis() - lastMeasurementTime < 10000) {
            final byte[] data = characteristic.getValue();
            if (data != null) {
                Log.d(TAG, "Received " + data.length + " bytes...");

                mSensorData = sBleDriver.processData(data, gatt.getDevice().getName(), gatt.getDevice().getAddress());
                if (mSensorData != null){//!mSensorData.getDataMap().isEmpty()) {
                    Log.d(TAG, "Valid data.");

                    lastMeasurementTime = System.currentTimeMillis();

                        BluetoothHelper.sensorDataPrintFormat_spo2(mSensorData);
                        BluetoothHelper.sensorDataPrintFormat_bpm(mSensorData);
                        BluetoothHelper.sensorDataPrintFormat_pi(mSensorData);
                        SensorsService.sendMessage(SensorsService.SENSORS_MEASURING, mSensorData);

                } else {
                    if (mSensorData == null) {
                        Log.d(TAG, "Invalid data: mSensorData == null)");
                    } else {
                        Log.d(TAG, "Invalid data: mSensorData empty");
                    }
                }
            }
        } else {
            //Post measurement and disconnect
            Log.i(TAG, "No data for the last seconds. Disconnecting...");
            BluetoothGattService gattService = gatt.getService(sBleDriver.getServiceUuid());
            if (gattService != null) {
                BluetoothGattCharacteristic gattCharacteristic =
                        gattService.getCharacteristic(sBleDriver.getCharacteristicUuid());
                if (gattCharacteristic != null) {
                    Log.i(TAG, "Setting Characteristic " + gattCharacteristic.getUuid());

                    //Disable notification
                    gatt.setCharacteristicNotification(gattCharacteristic, false);

                    BluetoothGattDescriptor clientDescriptor = gattCharacteristic.getDescriptor(
                            BluetoothHelper.CLIENT_CHARACTERISTIC_CONFIG);
                    clientDescriptor.setValue(sBleDriver.getDescriptor());
                    gatt.writeDescriptor(clientDescriptor);

                    BleDriver.GattData postData = sBleDriver.postMeasurement();
                    Log.i(TAG, "Check for Post measurement ");
                    if (postData != null) {
                        Log.i(TAG, " Post measurement Exists");
                        BluetoothGattCharacteristic gattCharacteristicSend = gattService.getCharacteristic(postData.SendCmd);
                        if (gattCharacteristicSend != null) {
                            Log.i(TAG, " Sending Post Measurement Cmd");
                            gattCharacteristicSend.setValue(postData.data);
                            gatt.writeCharacteristic(gattCharacteristicSend);
                        }
                    }
                }
            }
        }
    }
}

