package com.example.bleexperiments.sensors;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.example.bleexperiments.MainActivity;
import com.example.bleexperiments.R;
import com.example.bleexperiments.sensors.drivers.BleDriver;
import com.example.bleexperiments.sensors.helpers.BluetoothHelper;
import com.example.bleexperiments.sensors.helpers.Sensor;
import com.example.bleexperiments.sensors.helpers.SensorData;
import com.example.bleexperiments.sensors.managers.BleManager;

import java.util.Date;

public class SensorsService extends Service {
    public final static String TAG = SensorsService.class.getSimpleName();

    public final static String SENSOR_DATA = "com.example.bleexperiments.sensors.SensorsService.SENSOR_DATA";

    public final static String INTENT_SENSORS_START_SCAN = "com.example.bleexperiments.sensors.SensorsService.INTENT_SENSORS_START_SCAN";
    public final static String INTENT_SENSORS_STOP_ALL = "com.example.bleexperiments.sensors.SensorsService.INTENT_SENSORS_STOP_ALL";
    public final static String INTENT_SENSORS_SCAN_FAILED = "com.example.bleexperiments.sensors.SensorsService.INTENT_SENSORS_SCAN_FAILED";
    public final static String INTENT_SENSORS_CONNECTED = "com.example.bleexperiments.sensors.SensorsService.INTENT_SENSORS_CONNECTED";
    public final static String INTENT_SENSORS_MEASURING = "com.example.bleexperiments.sensors.SensorsService.INTENT_SENSORS_MEASURING";
    public final static String INTENT_SENSORS_DISCONNECTED = "com.example.bleexperiments.sensors.SensorsService.INTENT_SENSORS_DISCONNECTED";

    public final static int SENSORS_INACTIVE = 500;
    public final static int SENSORS_SCAN_BLE = 503;
    public final static int SENSORS_SCAN_FAILED = 505;
    public final static int SENSORS_SCAN_STOP = 506;
    public final static int SENSORS_STOP_ALL = 507;
    public final static int SENSORS_CONNECTED_BLE = 509;
    public final static int SENSORS_CONNECTING_BLE = 511;
    public final static int SENSORS_DISCONNECTED = 512;
    public final static int SENSORS_SERVICES_DISCOVERED = 513;
    public final static int SENSORS_MEASURING = 514;

    private static ServiceHandler sServiceHandler;
    private static HandlerThread sServiceHandlerThread;

    private BluetoothAdapter mBluetoothAdapter;
    private BleManager mBleManager;

    private final static int SCAN_BLE_PERIOD = 15000;
    private final static int CONNECTING_BLE_PERIOD = 8000;
    private CountDownTimer mScanTimer;

    private int mStatus;
    private int mPrevStatus;

    @Override
    public void onCreate() {
        Log.i(TAG, "SensorsService Created");

        BluetoothManager mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();

        sServiceHandlerThread = new HandlerThread("ServiceThread");
        sServiceHandlerThread.start();
        sServiceHandler = new ServiceHandler(sServiceHandlerThread.getLooper());

        mStatus = SENSORS_INACTIVE;
        mPrevStatus = SENSORS_INACTIVE;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        switch (intent.getAction()) {
            case INTENT_SENSORS_START_SCAN:
                Log.i(TAG, "SensorService onStartCommand: INTENT_SENSORS_START_SCAN");
                if (mStatus == SENSORS_INACTIVE) {
                    sServiceHandler.sendEmptyMessage(SENSORS_SCAN_BLE);
                }
                return START_STICKY;
            case INTENT_SENSORS_STOP_ALL:
                Log.i(TAG, "SensorService onStartCommand: INTENT_SENSORS_STOP_ALL");
                sServiceHandler.sendEmptyMessage(SENSORS_STOP_ALL);
                stopSelf();
                return START_NOT_STICKY;
            default:
                Log.i(TAG, "SensorService onStartCommand: UNKNOWN_COMMAND: " + intent.getAction());
                stopSelf();
                return START_NOT_STICKY;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "SensorService Destroyed");
        sServiceHandlerThread.quitSafely();
    }

    /**
     * Handler that receives messages from the thread
     */
    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            mStatus = msg.what;
            Sensor sensor = (Sensor) msg.obj;
            switch (msg.what) {
                case SENSORS_SCAN_BLE:
                    //Scan BLE sensors
                    Log.i(TAG, "Handler: Scanning for BLE sensors...");
                    startBluetoothScan();
                    setScanTimer(SENSORS_SCAN_BLE);
                    break;
                case SENSORS_SCAN_FAILED:
                    Log.i(TAG, "Handler: Scanning failed...");
                    mBluetoothAdapter.cancelDiscovery();
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    disconnect();
                    stopSelf();
                    broadcastStatus(INTENT_SENSORS_SCAN_FAILED);
                    break;
                case SENSORS_STOP_ALL:
                    Log.i(TAG, "Handler: Scanning stopped...");
                    mBluetoothAdapter.cancelDiscovery();
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    disconnect();
                    stopSelf();
                    break;
                case SENSORS_CONNECTING_BLE:
                    if (mPrevStatus == SENSORS_SCAN_BLE) {
                        Log.i(TAG, "Handler: Connecting BLE to device...");
                        connectBle(sensor.getDeviceAddress());
                    }
                    break;
                case SENSORS_CONNECTED_BLE:
                    Log.i(TAG, "Handler: Connected BLE to device...");
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    setScanTimer(SENSORS_CONNECTED_BLE);
                    broadcastStatus(INTENT_SENSORS_CONNECTED, sensor);
                    break;
                case SENSORS_SERVICES_DISCOVERED:
                    Log.i(TAG, "Handler: Services discovered...");
                    break;
                case SENSORS_MEASURING:
                    Log.i(TAG, "Handler: Measuring...");
                    broadcastStatus(INTENT_SENSORS_MEASURING, sensor);
                    break;
                case SENSORS_DISCONNECTED:
                    Log.i(TAG, "Handler: Disconnected...");
                    //Checks the case of reconnection during scan for registered
                    sServiceHandler.sendEmptyMessage(SENSORS_SCAN_FAILED);
                    break;
                default:
                    super.handleMessage(msg);
            }
            mPrevStatus = mStatus;
        }
    }

    private final BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(final BluetoothDevice device, int rssi,
                                     byte[] scanRecord) {
                    Log.i(TAG, "Found device " + device.getName() + " - " + device.getAddress());

                    String deviceName = device.getName();

                    if (deviceName != null && BluetoothHelper.getDriver(deviceName) != null) {
                        synchronized (SensorsService.class) {
                            Sensor sensor = new Sensor(
                                    deviceName,
                                    device.getAddress(),
                                    new Date());
                            sServiceHandler.obtainMessage(SENSORS_CONNECTING_BLE, sensor).sendToTarget();
                        }
                    }
                }
            };

    /**
     * Checks if bluetooth is available and enables it if it is disabled.
     */
    private synchronized boolean initializeBluetooth() {
        if (mBluetoothAdapter != null) {
            if (!mBluetoothAdapter.isEnabled()) {
                Toast.makeText(getApplicationContext(), R.string.enabling_bluetooth, Toast.LENGTH_SHORT).show();
                mBluetoothAdapter.enable();
            }
            return true;
        } else {
            Toast.makeText(getApplicationContext(), R.string.bluetooth_not_available, Toast.LENGTH_LONG).show();
            return false;
        }
    }

    /**
     * This method starts the bluetooth scan process.
     */
    private synchronized void startBluetoothScan() {
        if (initializeBluetooth()) {
            Log.i(TAG, "Scanning (BLE)...");
            mBluetoothAdapter.startLeScan(mLeScanCallback);
        }
    }

    /**
     * This method sets the timers for scanning.
     *
     * @param scanType The scan type to set the timer for or to disable the timer.
     */
    private synchronized void setScanTimer(int scanType) {
        //cancel counter if exists and start a new to stop scanning
        if (mScanTimer != null) {
            mScanTimer.cancel();
            mScanTimer = null;
        }

        switch (scanType) {
            case SENSORS_SCAN_BLE:
                mScanTimer = new CountDownTimer(SCAN_BLE_PERIOD, 1000) {
                    @Override
                    public void onFinish() {
                        Log.i(TAG, "Timer: onFinish for SENSORS_SCAN_BLE...");
                        if (mStatus != SENSORS_MEASURING) {
                            mBluetoothAdapter.stopLeScan(mLeScanCallback);
                            sServiceHandler.sendEmptyMessage(SENSORS_SCAN_FAILED);
                        }
                    }

                    @Override
                    public void onTick(long millisUntilFinished) {

                    }
                }.start();
                break;
            case SENSORS_CONNECTING_BLE:
                mScanTimer = new CountDownTimer(CONNECTING_BLE_PERIOD, 1000) {
                    @Override
                    public void onFinish() {
                        Log.i(TAG, "Timer: onFinish for SENSORS_CONNECTING_BLE...");
                        if (mStatus != SENSORS_CONNECTED_BLE &&
                                mStatus != SENSORS_MEASURING) {
                            Log.i(TAG, "Timer: onFinish for SENSORS_CONNECTING_BLE... in if");
                            disconnect();
                            sServiceHandler.sendEmptyMessage(SENSORS_SCAN_BLE);
                        }
                    }

                    @Override
                    public void onTick(long millisUntilFinished) {

                    }
                }.start();
                break;
            case SENSORS_SCAN_STOP:
                Log.i(TAG, "Timer Cancelled: SENSORS_SCAN_STOP");
                break;
            case SENSORS_CONNECTED_BLE:
                Log.i(TAG, "Timer Cancelled: SENSORS_CONNECTED_BLE");
                break;
        }
    }

    /**
     * Connect to a BLE device.
     *
     * @param deviceAddress The address of the BLE device to connect.
     */
    private synchronized void connectBle(String deviceAddress) {
        BluetoothDevice remoteDevice = mBluetoothAdapter.getRemoteDevice(deviceAddress);
        BleDriver bleDriver = BluetoothHelper.getDriver(remoteDevice.getName());

        if (bleDriver != null) {
            setScanTimer(SENSORS_CONNECTING_BLE);

            mBleManager = new BleManager(getApplicationContext());
            mBleManager.connect(remoteDevice, bleDriver);
        } else {
            Log.i(TAG, "BLE Device " + remoteDevice.getName() + " - " + remoteDevice.getAddress() + " not supported.");
        }
    }

    /**
     * Disconnect from the currently connected Bluetooth device.
     */
    private synchronized void disconnect() {
        setScanTimer(SENSORS_SCAN_STOP);
        if (mBleManager != null) {
            mBleManager.disconnect();
        }
    }

    /**
     * Sends a message to the service handler
     *
     * @param status     the status to send to the handler
     * @param sensorData the SensorData object to send to the handler
     */
    public static void sendMessage(int status, SensorData sensorData) {
        if (sServiceHandlerThread.isAlive()) {
            sServiceHandler.obtainMessage(status, sensorData).sendToTarget();
        }
    }

    public static void sendMessage2(int status, Sensor sensor) {
        if (sServiceHandlerThread.isAlive()) {
            sServiceHandler.obtainMessage(status, sensor).sendToTarget();
        }
    }

    /**
     * Broadcasts the sensor service status to the activity
     *
     * @param status The sensor status
     */
    private synchronized void broadcastStatus(String status) {
        Intent intent = new Intent(status);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }


    /**
     * Broadcasts the sensor service status to the activity
     *
     * @param status     The sensor status
     * @param sensor The sensor data if available
     */
    private synchronized void broadcastStatus(String status, Sensor sensor) {
        Intent intent = new Intent(status);
        intent.putExtra(SENSOR_DATA, sensor);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }
}
